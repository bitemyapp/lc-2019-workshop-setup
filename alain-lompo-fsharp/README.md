# Alain Lompo's F# Lambdas and Behaviors Parameterization

https://github.com/alainlompo/fsharp-functions-families-bundles/tree/feature/docker_config_template_solution

contains sample projects and basic code.

Using:

- .Net Framework 4.7.1 or newer versions
- F# 4.1 (Fsharp Core 4.4.1.0)
