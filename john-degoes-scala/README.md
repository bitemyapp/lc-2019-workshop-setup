# Scala/sbt installation guide

First you need a JDK installed. OpenJDK 8 or newer should be fine. Oracle JDK should also work fine.

## JDK installation

If you don't already have the Java development kit installed, you'll need it.

### Windows

https://stackoverflow.com/questions/52511778/how-to-install-openjdk-11-on-windows

https://jdk.java.net/12/ It's just a zip file, see the Stack Overflow instructions for more.

### Mac

If you don't already have Oracle's JDK installed:

https://installvirtual.com/install-openjdk-11-mac-using-brew/

### Linux

For Ubuntu users:

```
sudo apt-get install openjdk-11-jre openjdk-11-jdk
```

CentOS/Fedora: https://computingforgeeks.com/how-to-install-java-11-on-centos-7-fedora-29-fedora-28/

If you're using something less common than Ubuntu or Fedora you can probably find distro-specific instructions. If not, the Fedora instructions where you `mv` the jdk directory under `/usr/local` might work.

## Installing sbt

### Windows

https://www.scala-sbt.org/release/docs/Installing-sbt-on-Windows.html

### Mac

https://www.scala-sbt.org/release/docs/Installing-sbt-on-Mac.html

### Linux

https://www.scala-sbt.org/release/docs/Installing-sbt-on-Linux.html

# Docker guide

If you wish to use Docker instead of running the builds natively, you can use the setup we've provided. From the top level of the `lc-2019-docker-crew` repository, run the following to build the Docker image and then start a `bash` shell inside of the Docker environment:

```
make build-john-degoes-scala
make shell-john-degoes-scala
```

Keep in mind, the Docker image defaults to the `root` user so if you try to run a build from your native OS after `sbt` has built the project inside Docker you may need to plow away some build artifact directories owned by `root`.
