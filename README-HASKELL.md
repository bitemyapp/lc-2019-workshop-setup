# Getting Haskell installed

Cf. http://haskellstack.org

## Windows

From the Haskell Stack website, download the Windows installer. It's located at:

https://get.haskellstack.org/stable/windows-x86_64-installer.exe

## Mac

```
curl -sSL https://get.haskellstack.org/ | sh
```

## Linux

Linux distros will often have `wget` but not `curl` installed.

```
wget -qO- https://get.haskellstack.org/ | sh
```
