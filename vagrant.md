
## Using Virtualbox and Vagrant to run a Linux Ubuntu VM

One option for running the tools in a self-contained environment is a Linux VM.
Ubuntu makes for a simple and reliable platform to build the environment on, and
we can use VirtualBox and Vagrant to build the VM.

* Install Vagrant for your platform: https://www.vagrantup.com/downloads.html
* Install VirtualBox for your platform: https://www.virtualbox.org/wiki/Downloads

### Build the VM

```
$ vagrant up
```

### SSH into the VM

```
$ vagrant ssh
```

If you do not have Virtualbox or Vagrant available, and want to replicate the
setup, see the shell script embedded in the `Vagrantfile` in this repo for
details on what is installed.
