MAKEFLAGS = -s

.DEFAULT_GOAL = help

# -------------------------------------------------------------------------------------------------
##
### Variables
##
export LOCAL_USER_ID ?= $(shell id -u $$USER)
export CI_REPO ?= lc-2019-docker-crew

export ALAIN_LOMPO_FSHARP_NAME ?= alain-lompo-fsharp
export ALAIN_LOMPO_FSHARP ?= ${CI_REPO}/${ALAIN_LOMPO_FSHARP_NAME}
export ALAIN_LOMPO_FSHARP_REPO ?= fsharp-functions-families-bundles

export KAPIL_REDDY_CLOJURE_NAME ?= kapil-reddy-clojure
export KAPIL_REDDY_CLOJURE ?= ${CI_REPO}/${KAPIL_REDDY_CLOJURE_NAME}
export KAPIL_REDDY_CLOJURE_REPO = intermediate-clojure-workshop

export JOHN_DEGOES_SCALA_NAME ?= john-degoes-scala
export JOHN_DEGOES_SCALA ?= ${CI_REPO}/${JOHN_DEGOES_SCALA_NAME}
export JOHN_DEGOES_SCALA_REPO ?= zio-workshop

export MICHAEL_SNOYMAN_HASKELL_STM_DOCKER_IMG_NAME = ${CI_REPO}/michael-snoyman-haskell-stm
export LYLE_KOPNICKY_PROOF_VERIFIER_DOCKER_IMG_NAME = ${CI_REPO}/lyle-kopnicky-proof-verifier

export ALEXKNVL_NAME = alexknvl
export ALEXKNVL_RECURSION_REPO = recursion-schemes-and-final-tagless
export ALEXKNVL_RECURSION_PATH = ${ALEXKNVL_NAME}-${ALEXKNVL_RECURSION_REPO}
export ALEXKNVL_RECURSION_DOCKER_IMG = ${CI_REPO}/${ALEXKNVL_RECURSION_PATH}
export ALEXKNVL_PRACTICAL_REPO = practical-observable-sharing-talk
export ALEXKNVL_PRACTICAL_PATH = ${ALEXKNVL_NAME}-${ALEXKNVL_PRACTICAL_REPO}
export ALEXKNVL_PRACTICAL_DOCKER_IMG = ${CI_REPO}/${ALEXKNVL_PRACTICAL_PATH}

export TDIETERT_NAME = tdietert
export TDIETERT_REPO = types-as-specifications
export TDIETERT_PATH = ${TDIETERT_NAME}-${TDIETERT_REPO}
export TDIETERT_DOCKER_IMG = ${CI_REPO}/${TDIETERT_PATH}

export MLOCHBAUM_PATH = mlochbaum-OuterProduct
export MLOCHBAUM_DOCKER_IMG = dyalog/dyalog

require-%:
	if [ "${${*}}" = "" ]; then \
		echo "ERROR: Environment variable not set: \"$*\""; \
		exit 1; \
	fi

# jq version 1.5.1-a5b5cbe, in2csv 1.0.4

## use yq to query for status on the list of presenters/speakers
speaker-status:
	yq r status.yaml 'presenters.*.status'

## use yq to query for the list of repos via the list of presenters/speakers
repo-list:
	yq r status.yaml 'presenters.*.workshop-repo' | sed 's/- http/http/' | sed '/^[[:space:]]*$$/d' | grep -v null | grep -v '"- "' | head -n "-1" # the grep still leaves the trailing - ""

## use yq to query presenter/speaker email
speakers:
	yq r status.yaml 'presenters.*.email'

## use yaml2json and jq to query and filter for the list of speakers that have no repo
speakers-no-repo:
	yaml2json < status.yaml | jq '.presenters | map(select(.status == "no-repo"))' | jq 'map(.email)' | in2csv -f json

## use yaml2json and jq to query and filter for the list of speakers that have no repo
speakers-no-mvp:
	yaml2json < status.yaml | jq '.presenters | map(select(.status == "no-mvp"))' | jq 'map(.email)' | in2csv -f json

# -------------------------------------------------------------------------------------------------
##
### Building and running Docker images
##

## build Alain Lompo's fsharp workshop
build-alain-lompo-fsharp:
	@cd ${ALAIN_LOMPO_FSHARP_NAME} && docker build -t "${ALAIN_LOMPO_FSHARP}" .

## Run the build in the container
run-build-alain-lompo-fsharp:
	@cd ${ALAIN_LOMPO_FSHARP_NAME} && docker run -e COLUMNS="`tput cols`" -e LINES="`tput lines`" --mount type=bind,source=$$(pwd)/${ALAIN_LOMPO_FSHARP_REPO},target=/app -it "${ALAIN_LOMPO_FSHARP}:latest"

## Spawn bash shell in Alain Lompo's fsharp container
shell-alain-lompo-fsharp:
	@cd ${ALAIN_LOMPO_FSHARP_NAME} && docker run -e COLUMNS="`tput cols`" -e LINES="`tput lines`" --mount type=bind,source=$$(pwd)/${ALAIN_LOMPO_FSHARP_REPO},target=/app -it "${ALAIN_LOMPO_FSHARP}:latest" /bin/bash

## build Kapil Reddy's clojure workshop
build-kapil-reddy-clojure:
	@cd ${KAPIL_REDDY_CLOJURE_NAME} && docker build -t "${KAPIL_REDDY_CLOJURE}" .

## Run the build in the clojure workshop container
run-build-kapil-reddy-clojure:
	@cd ${KAPIL_REDDY_CLOJURE_NAME} && docker run -e COLUMNS="`tput cols`" -e LINES="`tput lines`" --mount type=bind,source=$$(pwd)/${KAPIL_REDDY_CLOJURE_REPO},target=/app -it "${KAPIL_REDDY_CLOJURE}:latest"

## Spawn bash shell in Kapil Reddy's clojure workshop
shell-kapil-reddy-clojure:
	@cd ${KAPIL_REDDY_CLOJURE_NAME} && docker run -e COLUMNS="`tput cols`" -e LINES="`tput lines`" --mount type=bind,source=$$(pwd)/${KAPIL_REDDY_CLOJURE_REPO},target=/app -it "${KAPIL_REDDY_CLOJURE}:latest" /bin/bash

## build John Degoes's scala workshop
build-john-degoes-scala:
	@cd ${JOHN_DEGOES_SCALA_NAME} && docker build -t "${JOHN_DEGOES_SCALA}" .

## Run the build in the scala workshop container
run-build-john-degoes-scala:
	@cd ${JOHN_DEGOES_SCALA_NAME} && docker run -e COLUMNS="`tput cols`" -e LINES="`tput lines`" --mount type=bind,source=$$(pwd)/${JOHN_DEGOES_SCALA_REPO},target=/app -it "${JOHN_DEGOES_SCALA}:latest"

## Spawn bash shell in John Degoes's scala workshop
shell-john-degoes-scala:
	@cd ${JOHN_DEGOES_SCALA_NAME} && docker run -e COLUMNS="`tput cols`" -e LINES="`tput lines`" --mount type=bind,source=$$(pwd)/${JOHN_DEGOES_SCALA_REPO},target=/app -it "${JOHN_DEGOES_SCALA}:latest" /bin/bash

## Build a docker image for Michael Snoyman's Haskell STM session
build-michael-snoyman-haskell-stm:
	docker build -t ${MICHAEL_SNOYMAN_HASKELL_STM_DOCKER_IMG_NAME} michael-snoyman-haskell-stm

## Spawn a shell in the docker image for Michael Snoyman's Haskell STM session
shell-michael-snoyman-haskell-stm:
	cd michael-snoyman-haskell-stm && docker run -e COLUMNS="`tput cols`" -e LINES="`tput lines`" --mount type=bind,source=$$(pwd)/why-you-should-use-stm,target=/app --mount type=bind,source=${HOME}/.stack,target=/root/.stack -it "${MICHAEL_SNOYMAN_HASKELL_STM_DOCKER_IMG_NAME}:latest" /bin/bash

## Build a docker image for Lyle Kopnicky's Haskell STM session
build-lyle-kopnicky-proof-verifier:
	docker build -t ${LYLE_KOPNICKY_PROOF_VERIFIER_DOCKER_IMG_NAME} lyle-kopnicky-proof-verifier

## Spawn a shell in the docker image for Lyle Kopnicky's Haskell Proof Verifier session
shell-lyle-kopnicky-proof-verifier:
	cd lyle-kopnicky-proof-verifier && docker run -e COLUMNS="`tput cols`" -e LINES="`tput lines`" --mount type=bind,source=$$(pwd)/,target=/app --mount type=bind,source=${HOME}/.stack,target=/root/.stack -it "${LYLE_KOPNICKY_PROOF_VERIFIER_DOCKER_IMG_NAME}:latest" /bin/bash

## Build a docker image for Alexander Konovalov's Recursion Schemes session
build-alexknvl-recursion-schemes:
	docker build -t ${ALEXKNVL_RECURSION_DOCKER_IMG} ${ALEXKNVL_RECURSION_PATH}

## Spawn a shell in the docker image for Alexander Konovalov's Recursion Schemes session
shell-alexknvl-recursion-schemes:
	cd ${ALEXKNVL_RECURSION_PATH} && docker run -e COLUMNS="`tput cols`" -e LINES="`tput lines`" --mount type=bind,source=$$(pwd),target=/app --mount type=bind,source=${HOME}/.stack,target=/root/.stack -it "${ALEXKNVL_RECURSION_DOCKER_IMG}:latest" /bin/bash

## Build a docker image for Alexander Konovalov's Practical Observable Sharing talk
build-alexknvl-practical-observable-sharing-talk:
	docker build -t ${ALEXKNVL_PRACTICAL_DOCKER_IMG} ${ALEXKNVL_PRACTICAL_PATH}

## Spawn a shell in the docker image for Alexander Konovalov's Recursion Schemes session
shell-alexknvl-practical-observable-sharing-talk:
	cd ${ALEXKNVL_PRACTICAL_PATH} && docker run -e COLUMNS="`tput cols`" -e LINES="`tput lines`" --mount type=bind,source=$$(pwd),target=/app --mount type=bind,source=${HOME}/.stack,target=/root/.stack -it "${ALEXKNVL_PRACTICAL_DOCKER_IMG}:latest" /bin/bash

## Build a docker image for Thomas Dietert's Haskell Types as Specifications session
build-tdietert-types-as-specifications:
	docker build -t ${TDIETERT_DOCKER_IMG} ${TDIETERT_PATH}

## Spawn a shell in the docker image for Thomas Dietert's Haskell Types as Specifications session
shell-tdietert-types-as-specifications:
	cd ${TDIETERT_PATH} && docker run -e COLUMNS="`tput cols`" -e LINES="`tput lines`" --mount type=bind,source=$$(pwd),target=/app --mount type=bind,source=${HOME}/.stack,target=/root/.stack -it "${TDIETERT_DOCKER_IMG}:latest" /bin/bash

pull-dyalog-docker-image:
	docker pull ${MLOCHBAUM_DOCKER_IMG}

shell-mlochbaum-OuterProduct:
	cd ${MLOCHBAUM_PATH} && docker run -it --rm -p 4502:4502 -e RIDE_INIT=http:*:4502 --mount type=bind,source=$$(pwd),target=/app ${MLOCHBAUM_DOCKER_IMG}

# -------------------------------------------------------------------------------------------------
##
### Elm Workshops
##

## Use wget and tar to download and unpack the Elm executable for linux
install-elm-linux:
	wget -O - https://github.com/elm/compiler/releases/download/0.19.0/binaries-for-linux.tar.gz | tar -f - -xz -C ~/bin
	chmod +x ~/bin/elm
	elm --help

## Use wget and tar to download and unpack the node, npm, and npx executables for linux
install-nodejs-and-npm-linux:
	wget -O - https://nodejs.org/dist/v10.16.0/node-v10.16.0-linux-x64.tar.xz | tar xJ -f - -C ~/bin
	ln -sf ~/bin/node-v10.16.0-linux-x64/bin/node ~/bin/node
	ln -sf ~/bin/node-v10.16.0-linux-x64/lib/node_modules/npm/bin/npm-cli.js ~/bin/npm
	ln -sf ~/bin/node-v10.16.0-linux-x64/lib/node_modules/npm/bin/npx-cli.js ~/bin/npx
	echo npm version && npm --version
	echo node version && node --version

# -------------------------------------------------------------------------------------------------
##
### F# Workshop
##

add-mono-repo-ubuntu-1804:
	# add mono repo
	sudo apt install gnupg ca-certificates
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
	echo "deb https://download.mono-project.com/repo/ubuntu stable-bionic main" | sudo tee /etc/apt/sources.list.d/mono-official-stable.list
	sudo apt update


install-fsharp-linux: add-mono-repo-ubuntu-1804
	sudo apt-get install fsharp

# -------------------------------------------------------------------------------------------------
##
### Haskell Workshops
##

## Use wget to install stack for linux
install-stack-linux:
	wget -qO- https://get.haskellstack.org/ | sh

## Use wget to install stack for macos
install-stack-macos:
	curl -sSL https://get.haskellstack.org/ | sh


# -------------------------------------------------------------------------------------------------
##
### Scala Workshops
##

## Use apt to install sbt for Ubuntu linux
install-scala-sbt-linux:
	echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
	sudo apt-get update
	sudo apt-get install sbt

## Use apt to install openjdk for Ubuntu linux
install-scala-oepnjdk-linux:
	sudo apt install openjdk-8-jdk-headless
	javac -version
	echo "      ^^^^^^ you should see 1.8.x here"

# -------------------------------------------------------------------------------------------------
##
### Random Helpers
##

## print env for debug
print-env:
	env | sort

## mkdir ~/bin
mkdir-bin:
	mkdir ~/bin

## mkdir ~/.stack
mkdir-dot-stack:
	mkdir ~/.stack

# -------------------------------------------------------------------------------------------------
##
### Install tools and dependencies (stuff you may or may not want)
##

## Use wget to install yq to ~/bin
install-yq:
	wget -O ~/bin/yq https://github.com/mikefarah/yq/releases/download/2.4.0/yq_linux_amd64
	chmod +x ~/bin/yq
	echo installed to
	which yq
	yq --help

## Follow Docker's Ubuntu Instructions to install on that platform - https://docs.docker.com/install/linux/docker-ce/ubuntu/
install-docker-linux-ubuntu:
	sudo apt-get update
	sudo apt-get install apt-transport-https \
	                ca-certificates \
	                curl \
	                gnupg-agent \
	                software-properties-common
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
	sudo apt-key fingerprint 0EBFCD88
	sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
	sudo apt-get update
	sudo apt-get install docker-ce docker-ce-cli containerd.io


# -------------------------------------------------------------------------------------------------
##
### VirtualBox
##

## Use vagrant to build a Linux VM environment running on VirtualBox
build-linux-vm:
	vagrant up

## Use vagrant and SSH to drop into that VM
ssh-vm:
	vagrant ssh

## Show help screen.
help:
	env echo -e "Please use \`make <target>' where <target> is one of\n"
	awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "%-30s %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)
