# WIP Instructions that are not yet ready

## Marshall Lochbaum's Dyalog APL workshop

These instructions may not work, as there seem to be some issues with character encoding.
Feel free to send us a merge request if you find out how to get that working.

This might fail with character encoding issues:

```
$ docker run -it --rm -p 4502:4502 -e RIDE_INIT='http:*:4502' -v $PWD/OuterProduct:/app dyalog/dyalog
```

See also:

```
$ make pull-dyalog-docker-image
$ make shell-mlochbaum-OuterProduct
```



