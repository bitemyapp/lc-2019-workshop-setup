# LambdaConf 2019 Session Setup Info

This README provides helpful info related to getting your computer setup for each of the sessions at LambdaConf 2019.

## F# Workshop

### Install mono and `fsharp` on MacOS

* [MacOS Install Instructions](https://fsharp.org/use/mac/)

### Install mono and `fsharp` on Ubuntu Linux

These instructions are based on the [upstream docs here](https://fsharp.org/use/linux/).

First, [add the `mono` apt repo](https://www.mono-project.com/download/stable/)

Then install `fsharp` with: `apt install fsharp`

We have a helper utility here: `make install-fsharp-linux`


## Workshops using Scala

The info here is sourced from the [upstream install docs](https://www.scala-sbt.org/1.0/docs/Setup.html)

[General docs on `sbt` can be found here](https://www.scala-sbt.org/1.x/docs/index.html)

After installing `sbt`, you should be able to run `sbt` and then at the sbt prompt: `~ compile` .

### Install `sbt` on MacOS

[See the upstream docs here](https://www.scala-sbt.org/1.0/docs/Installing-sbt-on-Mac.html).

Install with brew: `brew install sbt@1`

[Download and run the installer from here](https://piccolo.link/sbt-1.2.8.msi)

### Install `sbt` on Windows

[See the upstream docs here](https://www.scala-sbt.org/1.0/docs/Installing-sbt-on-Windows.html)

### Install `sbt` on Linux

[Docs are here](https://www.scala-sbt.org/download.html).

We have a helper: `make install-scala-sbt-linux`


## Java and JDK

Ada, Scala and other tools require the JDK - we recommend Oracle JDK or OpenJDK.

#### Install OpenJDK on MacOS

If you don't already have Oracle's JDK installed:

https://installvirtual.com/install-openjdk-11-mac-using-brew/

```
$ brew tap AdoptOpenJDK/openjdk
$ brew cask install adoptopenjdk11
```

#### Install OpenJDK on Windows

[Here are some instructions for installing OpenJDK on Windows](https://stackoverflow.com/a/52531093).

https://jdk.java.net/12/ It's just a zip file, see the Stack Overflow instructions for more details.


#### Install OpenJDK on Linux

For Ubuntu users:

```
$ sudo apt-get install openjdk-11-jre openjdk-11-jdk
```

CentOS/Fedora: https://computingforgeeks.com/how-to-install-java-11-on-centos-7-fedora-29-fedora-28/

If you're using something less common than Ubuntu or Fedora you can probably find distro-specific instructions. If not, the Fedora instructions where you `mv` the jdk directory under `/usr/local` might work.


## Haskell Workshops 

[Click here for the installation instructions for Haskell](./README-HASKELL.md)

## GHCJS for Augmenting Reality in Haskell

[See the speaker's instructions and their docker image](https://github.com/fizruk/lambdaconf-2019-workshop/blob/master/PREPARE.md)

Note that in our tests with stock ubuntu 18.04, there are two issues you will likely run into:

* Install `ifconfig`
* Make sure you have added yourself to the docker group


## Workshops using Elm

The info here is sourced from the [upstream install docs](https://guide.elm-lang.org/install.html)

**Elm requires NPM, see the notes below on that**

### MacOS

[There is an installer](https://github.com/elm/compiler/releases/download/0.19.0/installer-for-mac.pkg)

### Windows

[There is also an installer](https://github.com/elm/compiler/releases/download/0.19.0/installer-for-windows.exe)

### Linux

Executables are packaged [here](https://github.com/elm/compiler/releases/download/0.19.0/binaries-for-linux.tar.gz).

In this repo we have a helper utility that will install these to `~/bin/`: `make install-elm-linux`

The package is sourced from https://github.com/elm/compiler/releases/.


## Workshops using NPM

Nodejs and the Node Package Manager are required for the Elm workshop.

The info here is sourced from the [upstream install docs](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm).

WARNING: The docs and other information available on the internet are confusing and suggest or recommend many different methods and tools.

For more info, see also [this post](https://blog.npmjs.org/post/85484771375/how-to-install-npm).

### Try these installers first

[For windows, macos, and linux](https://nodejs.org/en/download/)


## TLA Workshop

Attendees need to obtain TLA+ Toolbox which is
[available here](http://nightly.tlapl.us/products/) 
You can verify that TLA+ Toolbox is properly installed, if you can run it and
see an IDE that
[looks like this](https://learntla.com/introduction/example/img/intro_toolbox.png)

Note: On a fresh Linux Ubuntu install, we have only been able to get the nightly
TLA Plus release, and have not been able to get the v1.5.7 release running with
the latest JDK. 

[Download the nightly releases for MacOS, Windows, and Linux here](http://nightly.tlapl.us/products/) 

Optionally you can also install following additional tools:
* [Graphviz](https://www.graphviz.org/) - for state machine visualization
  * use the `dot` executable on the `PATH`
- [pdflatex](https://en.wikipedia.org/wiki/PdfTeX) to generate PDF versions of the spec

### Dyalog APL

This session does not have a lot of interactive code to explore, but the speaker will
demonstrate the IDE and related tools.

Note that while there is an upstream docker image from dyalog, we have not yet
seen it work on linux, so YMMV. At the end of this README there are some WIP
instructions related to running the docker image.

---

## Speaker Repositories 

* [John DeGoes' Scala workshop]()


